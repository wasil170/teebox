﻿/*** 
GLOBAL SCRIPTS 
Do site wide initialization from here
***/

$(function () {
	teebox.mobilenav.init();

	$( ".text-show" ).each(function() {
	    $(this).on("click", function(){
	    	var getTargetSect = $(this).parent().attr('data-target');
	    	$(this).parent().addClass('content-shown');
	    	$('#'+getTargetSect).slideDown();
	    });
	});
	$( ".text-hide" ).each(function() {
	    $(this).on("click", function(){
	    	var getTargetSect = $(this).parent().attr('data-target');
	    	$(this).parent().removeClass('content-shown');
	    	$('#'+getTargetSect).slideUp();
	    });
	});

});
