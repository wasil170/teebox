﻿/*** 
PRODUCT SCRIPTS 
***/

$(function () {
    $('#zoom_product_image').elevateZoom({
        zoomType: "window",
        scrollZoom : false,
        zoomWindowWidth: '100%',
        zoomWindowHeight: 522,
        borderSize: 1,
        borderColour: "#edeff3",
        appendTo: ".product-top-details-section"
    }); 

	$('#zoom_product_image').mouseenter(function(){
		$('.target-section .zoomContainer').addClass('active');
	}).mouseleave(function(){
		$('.target-section .zoomContainer').removeClass('active');
	});



});

$(window).scroll(function() {
    if($('.product-image-section').length) {
        if ($(window).scrollTop() > $('.product-image-section').position().top) {
            $('.product-image-section').addClass('fixed');
        }
        if ($(window).scrollTop() < $('.product-image-section').position().top) {
            $('.product-image-section').removeClass('fixed');
        }
    }
});