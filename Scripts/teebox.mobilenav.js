﻿var teebox = teebox || {};

(function () {


/* Public functions */

    this.init = function () {

        $(document).on('click', ".js-mobile-menu-button" , function() {
            openMenu();
        } );

        $(document).on('click', ".js-mobile-menu-close-button" , function() {
            closeMenu();
        } );

        toggleMenu();

    };


    function openMenu() {
        document.getElementById("js-mobile-nav").style.height = "100%";
    }

    function closeMenu() {
        document.getElementById("js-mobile-nav").style.height = "0%";
    }

    function toggleMenu() {

        $(document).on('click', ".js-mobile-nav__toggle-level" , function() {
            var btn = $(this);
            var subMenu = btn.next("ul");
            var parent = btn.parent();
            if (parent.hasClass("active")) {
                subMenu.slideUp(function () {
                    parent.removeClass('active');
                });
            } else {
                btn.closest('ul').children('.active').each(function () {
                    var sibblingItem = $(this);
                    sibblingItem.children('ul').slideUp(function () {
                        sibblingItem.removeClass('active');
                    });
                });
                subMenu.slideDown(function () {
                    parent.addClass('active');
                });
            }
        });
    }


}).apply(teebox.mobilenav = teebox.mobilenav || {});
